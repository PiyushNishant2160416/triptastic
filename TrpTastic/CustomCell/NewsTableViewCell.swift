//
//  NewsTableViewCell.swift
//  TrpTastic
//
//  Created by Piyush on 16/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var newsImageView: UIImageView!
    
    @IBOutlet weak var newsTitleLabel: UILabel!
    
    @IBOutlet weak var newsDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /**
     get cell
     - parameter no parameter:
     - returns :  No value is return
     */
    func getTripCell(){
        configureTripCell()
        updateTripCell()
    }
    private func configureTripCell(){
        Helper.setCornerRadious(radius: 8, view: self.newsImageView)
        
    }
    private func updateTripCell(){
        newsTitleLabel.text = "News Title"
        newsDescriptionLabel.text = "News Description"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
