//
//  TripListTableViewCell.swift
//  TrpTastic
//
//  Created by Piyush on 16/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class TripListTableViewCell: UITableViewCell {

    @IBOutlet weak var tripImageView: UIImageView!
    
    @IBOutlet weak var triptitleLabel: UILabel!
    
    @IBOutlet weak var tripDescriptionLabel: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     get cell
     - parameter no parameter:
     - returns :  No value is return
    */
    func getTripCell(){
        configureTripCell()
        updateTripCell()
    }
    private func configureTripCell(){
        Helper.setCornerRadious(radius: 8, view: self.tripImageView)
        
    }
    private func updateTripCell(){
        triptitleLabel.text = "Trip Title"
        tripDescriptionLabel.text = "Description"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
