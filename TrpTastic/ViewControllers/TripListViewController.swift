//
//  TripListViewController.swift
//  TrpTastic
//
//  Created by Piyush on 16/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class TripListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configure()
    }
    private func configure(){
        //self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.topItem?.title = Constant.viewciontrollerTitle.TripListTitle
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Extension for tableviewDelgate and data source
extension TripListViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tripcell = tableView.dequeueReusableCellWithIdentifier(Constant.tableviewcellIdentifier.triplistcellIdentifier) as? TripListTableViewCell
        tripcell?.getTripCell()
        return tripcell!
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var sectionTitle = ""
        switch section {
        case 0:
            sectionTitle = "Created"
        case 1 :
            sectionTitle = "Joined"
        case 2:
            sectionTitle = "Public"
        default:
            sectionTitle = "Created"
        }
        
        
        let sectionView = UIView()
        sectionView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 20)
        sectionView.backgroundColor = Constant.color.navigationBarTintColor
        
        let sectionLabel = UILabel()
        sectionLabel.frame = CGRectMake(10, 5,tableView.bounds.size.width - 20 , 18)
        sectionLabel.font = UIFont.boldSystemFontOfSize(16.0)
        sectionLabel.text = sectionTitle
        sectionLabel.textColor = UIColor.whiteColor()
        sectionView.addSubview(sectionLabel)
        
        
        
        return sectionView
    }

}