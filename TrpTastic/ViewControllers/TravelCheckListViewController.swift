//
//  TravelCheckListViewController.swift
//  TrpTastic
//
//  Created by Piyush on 16/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class TravelCheckListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
    }

    
    private func configure(){
        //self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.navigationBar.topItem?.title = Constant.viewciontrollerTitle.travelChecklistcontrollerTitle
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TravelCheckListViewController:UITabBarDelegate,UITableViewDataSource{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let checklistCell = tableView.dequeueReusableCellWithIdentifier(Constant.tableviewcellIdentifier.travelChecklistIdentifier) as? TravelCheckListTableViewCell
        return checklistCell!
    }
}
