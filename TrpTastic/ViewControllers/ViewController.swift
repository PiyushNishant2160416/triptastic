//
//  ViewController.swift
//  TrpTastic
//
//  Created by Piyush on 14/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginorcreateaccountlabel: UILabel!
    
    @IBOutlet weak var emailIdTextField: UITextField!
    
    @IBOutlet weak var passwordtextfield: UITextField!
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    
    @IBOutlet weak var forgetbuttonOutlet: UIButton!
    
    @IBOutlet weak var createAccountbuttonOutlet: UIButton!
    
    @IBOutlet weak var emailProfileImage: UIImageView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    
    @IBAction func logInbuttonIsTapped(sender: UIButton) {
        let storyboard = UIStoryboard(name: Constant.storyboard.tripstoryboardIdentifier, bundle: nil)
        let triplistviewcontroller = storyboard.instantiateViewControllerWithIdentifier(Constant.viewcontrollerStoryboardId.triplistMainNavigationController) as? UINavigationController
        triplistviewcontroller?.navigationBar.barTintColor = Constant.color.navigationBarTintColor
        
//        
//        let storyboard = UIStoryboard(name: Constant.storyboard.newsStoryboardIdentifier, bundle: nil)
//        let newslistviewcontroller = storyboard.instantiateViewControllerWithIdentifier(Constant.viewcontrollerStoryboardId.newsListMainnavigationControoler) as? UINavigationController
//        newslistviewcontroller?.navigationBar.barTintColor = Constant.color.navigationBarTintColor

        
        
        
        presentViewController(triplistviewcontroller!, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       configureUserInterface()
       let backgroundImage = UIImage(named: "background.jpg")
        self.applyBackgroundImage(image: backgroundImage, imageView: backgroundImageView, imageViewContentMode: UIViewContentMode.ScaleAspectFill)
        
    }
    /**
     set Background image
     - parameter image,imageView : Image and Image view reference
     - returns : No value is return
    */
    func applyBackgroundImage(image image: UIImage?,imageView:UIImageView,imageViewContentMode:UIViewContentMode){
        backgroundImageView.contentMode = imageViewContentMode
        imageView.image = image
        
    }
    /**
     Change UI look and feel
     - parameter No parameter:
     - returns : No value is return
    */
    private func configureUserInterface(){
        let width = self.emailProfileImage.bounds.size.width + 10
        
        let emailLeftPadding = UIView(frame: CGRectMake(0, 0, width, self.emailIdTextField.frame.size.height))
        emailIdTextField.leftView = emailLeftPadding
        emailIdTextField.leftViewMode = UITextFieldViewMode .Always
        
        let passwordLeftPadding = UIView(frame: CGRectMake(0, 0, width, self.passwordtextfield.frame.size.height))
        passwordtextfield.leftView = passwordLeftPadding
        passwordtextfield.leftViewMode = UITextFieldViewMode .Always
        
        
        loginButtonOutlet.backgroundColor = Constant.color.redButtonBackgroundColor
        createAccountbuttonOutlet.backgroundColor = Constant.color.lightBlueButtonBackgroundColor
        
        Helper.setCornerRadious(radius: 15, view: loginButtonOutlet)
        Helper.setBorderwidth(width: 3, view: loginButtonOutlet)
        Helper.setCornerRadious(radius: 6, view: createAccountbuttonOutlet)
        Helper.setBorderwidth(width: 3, view: createAccountbuttonOutlet)
        let color = UIColor(red: 255, green: 255, blue: 255, alpha: 0)
        Helper.setBorderColor(color: color, view: createAccountbuttonOutlet)
        Helper.setBorderColor(color: color, view: loginButtonOutlet)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

