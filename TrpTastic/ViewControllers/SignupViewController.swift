//
//  SignupViewController.swift
//  TrpTastic
//
//  Created by Piyush on 15/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    @IBOutlet weak var userNameTextfield: UITextField!
    
    @IBOutlet weak var ageTextfield: UITextField!
    
    @IBOutlet weak var emailAddressTextfield: UITextField!
    
    @IBOutlet weak var dateofbirthTextfield: UITextField!
    
    
    @IBOutlet weak var genderTextfield: UITextField!
    
    @IBOutlet weak var passwordtextfield: UITextField!
    
    
    @IBOutlet weak var confirmpasswordtextfield: UITextField!
    
    @IBOutlet weak var termacceptbuttonOutlet: UIButton!
    
    @IBOutlet weak var signUpbuttonOutlet: UIButton!
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    
    @IBAction func signUpButtonIsTapped(sender: UIButton) {
    }
    
   
    
    
    
    
    private var dateofbirth = DatePicker()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.configureUserInterface()

        // Do any additional setup after loading the view.
    }
    /**
     Change UI look and feel
     - parameter No parameter:
     - returns : No value is return
     */
    private func configureUserInterface(){
        loginButtonOutlet.backgroundColor = Constant.color.redButtonBackgroundColor
        signUpbuttonOutlet.backgroundColor = Constant.color.lightBlueButtonBackgroundColor
        Helper.setCornerRadious(radius: termacceptbuttonOutlet.frame.size.width/2, view: termacceptbuttonOutlet)
        Helper.setBorderwidth(width: 1, view: termacceptbuttonOutlet)
        Helper.setCornerRadious(radius: 8, view: signUpbuttonOutlet)
        Helper.setCornerRadious(radius: 15, view: loginButtonOutlet)
        dateofbirth.setDatePicker(dateofbirthTextfield, datePickerMode: .Date)
        dateofbirth.setMinimumDate(NSDate())
        dateofbirth.doneButtonClicked = { (datePicker: UIDatePicker, textField: UITextField) -> Void in
            self.dateofbirthTextfield.text = Helper.formatDate(datePicker)
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
