//
//  DatePicker.swift
//  TrpTastic
//
//  Created by Piyush on 20/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

public class DatePicker: NSObject {
    
    var textField:UITextField!
    var datePickerView:UIDatePicker = UIDatePicker()
    var doneButtonClicked: ((datePicker: UIDatePicker, textField: UITextField) -> Void)?
    
    func setDatePicker(sender: UITextField,datePickerMode:UIDatePickerMode) {
        
        textField = sender
        datePickerView.datePickerMode = datePickerMode
        
        textField.inputView = datePickerView
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let toolBar = UIToolbar()
        let cancelButtonDate = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DatePicker.cancelButtonTapped))
        let doneButtonDate = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DatePicker.doneButtonTapped))
        toolBar.barStyle = UIBarStyle.Default
        toolBar.translucent = true
        toolBar.tintColor = UIColor.darkGrayColor()
        toolBar.sizeToFit()
        toolBar.setItems([cancelButtonDate,spaceButton,doneButtonDate], animated: false)
        toolBar.userInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar
    }
    
    
    func setMinimumDate(date: NSDate) {
        
        datePickerView.minimumDate = date
    }
    
    func doneButtonTapped(){
        
        doneButtonClicked?(datePicker:datePickerView,textField:textField)
        textField.resignFirstResponder()
    }
    
    func cancelButtonTapped(){
        
        textField.resignFirstResponder()
    }
}

/* func datePickerValueChanged(sender:UIDatePicker) {
 
 let dateFormatter = NSDateFormatter()
 
 dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
 
 dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
 
 textField.text = dateFormatter.stringFromDate(sender.date)
 }*/



