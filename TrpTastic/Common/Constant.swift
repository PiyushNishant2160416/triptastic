//
//  Constant.swift
//  TrpTastic
//
//  Created by Piyush on 16/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
import UIKit
public final class Constant{
    
    //Story Board Id
    struct storyboard {
        static let mainstoryboardIdentifier = "Main"
        static let tripstoryboardIdentifier = "Trip"
        static let newsStoryboardIdentifier = "News"
    }
    //Color Constants
    struct color{
        static let redButtonBackgroundColor = UIColor(hexString: "#DB4646FF")
        static let lightBlueButtonBackgroundColor = UIColor(hexString: "#1DA1BFFF")
        static let navigationBarTintColor = UIColor(hexString: "#4FB7C1FF")
    }
    
    // ViewControlles Storyboard Id
    struct viewcontrollerStoryboardId {
        static let loginviewcontroller = "ViewController"
        static let signupviewcontroller = "SignupViewController"
        static let termAndconditionviewcontroller = "TermAndConditionViewController"
        static let triplistviewcontroller = "TripListViewController"
        static let triplistMainNavigationController = "TripListMainNavigationcontroller"
        static let tripdetailViewcontroller = "TripDetailViewController"
        static let newsviewcontroller = "NewsViewController"
        static let newsListMainnavigationControoler = "NewsListMainNavigationcontroller"
        static let newsDetailViewcontroller = "NewsDetailViewController"
        static let travelCheckListviewcontroller = "TravelCheckListViewController"
    }
    //Table view Custom cell Identifier
    struct tableviewcellIdentifier {
        static let triplistcellIdentifier = "triplistcell"
        static let newscellIdentifier = "newscell"
        static let travelChecklistIdentifier = "travelchecklist"
        
    }
    //Viewcontroller Title
    struct viewciontrollerTitle {
        static let TripListTitle = "Trip List"
        static let newsControllerTitle = "News"
        static let travelChecklistcontrollerTitle = "Travel Check List"
    }
    
}