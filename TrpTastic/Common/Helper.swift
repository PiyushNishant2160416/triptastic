//
//  Helper.swift
//  TrpTastic
//
//  Created by Piyush on 20/06/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.startIndex.advancedBy(1)
            let hexColor = hexString.substringFromIndex(start)
            
            if hexColor.characters.count == 8 {
                let scanner = NSScanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexLongLong(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}


//Extension for UITextFields
extension UITextField
{
    public override func drawRect(rect: CGRect) {
        //setBottomBorder()
    }
    
    
    func setBottomBorder()
    {
        let sc = UIScreen.mainScreen()
        print("screen=\(sc.bounds.width)")
        print(self.frame.size.width)
        self.borderStyle = UITextBorderStyle.None;
        let border = CALayer()
        let width = CGFloat(2.5)
        border.borderColor = UIColor.blackColor().CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
}



public class Helper{
    /**
     Set corner Radious of View
     - parameter radius : CGFloat reference
     - returns :  No value is return
     */
    static func setCornerRadious(radius radius : CGFloat = 6,view:UIView){
        
        view.layer.cornerRadius = radius
    }
    /**
     Set Border width of View
     - parameter width : CGFloat reference
     - returns :  No value is return
     */
    static func setBorderwidth(width width : CGFloat = 6,view:UIView){
        view.layer.borderWidth = width
    }
    /**
     Set Border color of View
     - parameter color : UIColor reference
     - returns :  No value is return
     */
    static func setBorderColor(color color:UIColor = UIColor.whiteColor(),view:UIView){
        view.layer.borderColor = color.CGColor
        //view.layer.bo
    }
    /**
     set color and other properties
     - parameter No parameter:
     - returns : no value is return
     */
    //    static func setNaviagtionBarBackgroundColor(viewcontroller viewcontroller : UIViewController){
    //        viewcontroller.navigationController?.navigationBar.barTintColor = UIColor(hexString: Constant.Color.dardkGray)
    //        viewcontroller.navigationController?.navigationBar.tintColor = UIColor(hexString: Constant.Color.whitecolor)
    //    }
    /**
     Convert String formate date into NSdate
     */
    static func getNsdateFromDateString(dateString dateString:String) -> NSDate{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(dateString) as NSDate!
        
        //print(dateValue)
        return dateValue
        
    }
    static func getDateStringFromNsdate(nsdate nsdate:NSDate) -> String{
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        return dateFormatter.stringFromDate(nsdate)
    }
    
    static func getViewControllerInstanceUsingStoryboardID(storyboardName storyboardName:String,viewcontrollerId:String) -> UIViewController{
        let storyboard = UIStoryboard(name: storyboardName, bundle:nil)
        let viewControllerObject = storyboard.instantiateViewControllerWithIdentifier(viewcontrollerId)
        return viewControllerObject
        
    }
    /**
     Date from UIDate picker
     - parameter sender : UIDatePicker
     - returns : No value is return
     */
    static  func formatDate(sender: UIDatePicker) -> String {
        
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        
        return dateFormatter.stringFromDate(sender.date)
    }
    
    
}
